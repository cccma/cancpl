CanCPL Documentation
--------------------

***********
Description
***********
The CanCPL documentation uses a combination of Doxygen and Sphinx to generate
its documentation. The Fortran source code for the coupler has been partly
documented. High level documentation describing some of the design of the
coupler is written in restructured text.

Credit to @angus-g, @marshallward, and @jr3cermak for spearheading the effort
to document the github.com/mom-ocean/MOM6 project.
************
Instructions
************

++++++++++++++
One-time setup
++++++++++++++
Install sphinx and the necessary extensions by running::
  pip install -r requirements.txt

+++++++++++++++++++++++
Build the documentation
+++++++++++++++++++++++
A standard Sphinx ``makefile`` is included here. To build HTML webpages
similar to how they would appear on cancpl.readthedocs.io, type::
  make html

This will create a ``_build`` directory which contains the rendered documentation.

+++++++++++++++++++++++++++++++++++++++++
Previewing the documentation (CCCma only)
+++++++++++++++++++++++++++++++++++++++++

If you have access to the internal science network, you can host the documentation
internally using ``goc-dx``. Simply copy the ``_build/html`` directory from the
previous step into ``~/public_html/CanCPL/``. You should then be able to access
the site from `http://goc-dx.science.gc.ca/~USERNAME_HERE/CanCPL/`
